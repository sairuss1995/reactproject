const UPDATE_NEW_MESSAGE_BODY = 'UPDATE_NEW_MESSAGE_BODY';
const SEND_MESSAGE = 'SEND_MESSAGE';

let initialState =  {
    dialogs: [
    {id: 1, name: 'Dimych'},
    {id: 2, name: 'Andrey'},
    {id: 3, name: 'Sveta'},
    {id: 4, name: 'Sasha'},
    {id: 5, name: 'Viktor'},
    {id: 6, name: 'Valera'},],
messages: [
    {id: 1, message: 'HI'},
    {id: 2, message: 'How is your name'},
    {id: 3, message: 'My name Viktor'},
  ],
    newMessageBody: "",
};

const dialogsReducer = (state = initialState,action) => {
         switch (action.type) {
             case UPDATE_NEW_MESSAGE_BODY:
                state.newMessageBody = action.body;
                return state;
                case SEND_MESSAGE:
                    let body = state.newMessageBody;
                    state.newMessageBody = '';
                    state.messages.push({id: 4, message: body },);
             default:
                return state;
         }
}

export const sendMessageCreator = () => ({type: SEND_MESSAGE})
export const updateNewMessageBodyCreator = (text) => ({type: UPDATE_NEW_MESSAGE_BODY, body: text });
 
export default  dialogsReducer;