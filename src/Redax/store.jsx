import dialogsReducer from "./dialogs-reducer";
import profaileReducer from "./profaile-reducer";
import sidebarReducer from "./sidebar-reducer";
let store ={
  _state: {
    profailePage:{posts: [{id: 1, message: 'Hi, how are you', LikesCount: 74},{id: 1, message: 'It\'s my frist post', LikesCount: 97}],newPostText: 'Creeate post'},
    dialogsPage: {dialogs: [{id: 1, name: 'Dimych'},{id: 2, name: 'Andrey'},{id: 3, name: 'Sveta'},{id: 4, name: 'Sasha'},{id: 5, name: 'Viktor'},{id: 6, name: 'Valera'},],
    sidebar:{},
   messages: [{id: 1, message: 'HI'},{id: 2, message: 'How is your name'},{id: 3, message: 'My name Viktor'},],
    newMessageBody: "",
    }
},
getState(){return this._state;},
 _callSubscriber () {console.log('state');},
   subscribe  (observer) {this._callSubscriber = observer;},
    dispatch (action){
      this._state.profailePage = profaileReducer( this._state.profailePage,action);
      this._state.dialogsPage = dialogsReducer( this._state.dialogsPage,action);
      this._state.sidebar = sidebarReducer( this._state.sidebar,action);
      this._callSubscriber(this._state);

    
    }
}
export default store;
window.store = store;
  