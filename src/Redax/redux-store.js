const { combineReducers, createStore } = require("redux");
import {combineReducers, createStore} from "Redux";
import profaileReducer from "./profaile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

let reducers = combineReducers({
    profilePage:profaileReducer,
    dialogsPage:dialogsReducer,
    sidebar:sidebarReducer
});
let store = createStore(reducers);

export default store;