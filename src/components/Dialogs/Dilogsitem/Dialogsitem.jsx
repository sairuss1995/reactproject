import React from 'react';
import './../Dialogs.css';
import { NavLink } from 'react-router-dom';


const DialogItem = (props) => {
    let path = "/Dialogs/" + props.id;
    return (
        <div className='item'>
        <NavLink to={path}>{props.name}</NavLink>
 </div>
    );
}

export default DialogItem;