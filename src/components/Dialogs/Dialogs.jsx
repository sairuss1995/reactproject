import React from 'react';
import './Dialogs.css';
import DialogItem from './Dilogsitem/Dialogsitem';
import Message from './Message/Messageitem';
import {updateNewMessageBodyCreator, sendMessageCreator} from "../../Redax/dialogs-reducer";

const Dialogs = (props) =>{
    let state = props.store.getState().dialogsPage;
    let dialogsElements = state.dialogs.map(d =>  <DialogItem name={d.name} id={d.id}/>);
    let messagesDialogs = state.messages.map (m => <Message message={m.message}/>);
    let newMessageBody  = state.newMessageBody;

    let onMessageCleck = () =>{
        props.store.dispatch(sendMessageCreator());
    }

    let onNewMessageChange = (e) =>{
        let body = e.target.value;
         props.store.dispatch(updateNewMessageBodyCreator(body));
        }

    
    return(
        <div className='dialogs'>
            <div className="dialogs-item">
               {dialogsElements} 
            </div>
            <div className='messages'>
              <div> {messagesDialogs}</div>
              <div>
                  <div><textarea onChange={onNewMessageChange} value={newMessageBody } placeholder='Enter your message!'></textarea></div>
                  <div><button onClick={onMessageCleck}>Send</button></div>
              </div>
            </div>
        </div>

    );
}
export default Dialogs;