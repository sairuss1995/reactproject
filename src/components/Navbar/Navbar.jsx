import React from 'react';
import './Navbar.css';
import { NavLink } from 'react-router-dom';

const Navbar = () =>{
    return (
        <nav className='nav'>
        <div className='nevColor'>
          <NavLink to='/profaile' activeClassName="active" >Profaile</NavLink>
        </div>
        <div className='nevColor'>
          <NavLink to='dialogs'>Messages</NavLink>
        </div>
        <div className='nevColor'>
          <a>News</a>
        </div>
        <div className='nevColor'>
          <a>Music</a>
        </div>
        <div className='nevColor'>
          <a>Settings</a>
        </div>
      </nav>
      
    );
}
export default Navbar;