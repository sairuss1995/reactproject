import './Profaile.css';

import React from 'react';

import MyPosts from './MyPosts/MyPosts.jsx';
import ProfaileInfo from './Profaileinfo/Profaileinfo.jsx';

const Profaile = (props) =>{
    return (
      <div>
      <ProfaileInfo/>
      <MyPosts 
      posts={props.profailePage.posts} 
      newPostText={props.profailePage.newPostText}
      dispatch={props.dispatch}/>
      </div>
    );
}
export default Profaile;