import './MyPosts.css';

import React from 'react';

import Post from './Post/Post.jsx';
import { updatePostCreator,addPostCreator } from '../../../Redax/profaile-reducer';
 



const MyPosts = (props) => {
  let postsElement = 
  props.posts.map (p => <Post message={p.message} LikesCount={p.LikesCount}/>);

  let newPostElemet = React.createRef();
let addPost = () => {
   props.dispatch(addPostCreator());
  }
  let onPostChange = () =>{
    let text = newPostElemet.current.value;
   let action = updatePostCreator (text)
   props.dispatch (action);
   
  }
    return (
       
      <div className="postsblock">
        <h3>My posts</h3>
        <div>
          <div>
          <textarea ref={newPostElemet} value={props.newPostText}  onChange={onPostChange} />
          </div>
          <div>
          <button  onClick={addPost}>Add post </button>
          </div>
          <button > Remove</button>
        </div>
        <div className="posts">
          {postsElement}
       
        </div>
      </div>
      
    );
}
export default MyPosts;