import './App.css';

import React from 'react';

import { Route } from 'react-router-dom';

import Dialogs from './components/Dialogs/Dialogs';
import Header from './components/Header/Header.jsx';
import Navbar from './components/Navbar/Navbar.jsx';
import Profaile from './components/Profaile/Profaile.jsx';

const App = (props)  => {

 
  return (
    
    <div className="app-wrapper">
      <Header/>
      <Navbar/>
      <div className='app-wrapper-content'>
        <Route path='/dialogs' render={ () => <Dialogs store={props.store} />}/>
        <Route path='/profaile' render={ () => <Profaile profailePage={props.state.profailePage} dispatch={props.dispatch}  />}/>
      </div> 
    </div>
     );
}

export default App;
